using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using FlaviusJosephus;

namespace FlaviusJosephus.Tests
{
    public class Tests
    {
        private static IEnumerable<TestCaseData> CaseData
        {
            get
            {
                yield return new TestCaseData(
                    new List<int>()
                    {
                        1, 2, 3, 4,5, 6,
                    },
                    2,
                    new int[]
                    {
                        2, 4, 6, 3,1,
                    });

                yield return new TestCaseData(
                    new List<int>()
                    {
                        1, 2, 3, 4,5, 6,
                    },
                    3,
                    new int[]
                    {
                        3, 6, 4, 2,5,
                    });

                yield return new TestCaseData(
                    new List<int>()
                    {
                        1, 2, 3, 4,5, 6,
                    },
                    4, 
                    new int[]
                    {
                        4, 2, 1, 3,6, 
                    });
            }
        }

        [TestCaseSource(nameof(CaseData))]
        public void Test(List<int> collection, int next, int[] expected)
        {
            var flaviusJosephus = new FlaviusJosephus<int>(collection, next);
            var actual = flaviusJosephus.GetIEnumerable().ToArray();
            CollectionAssert.AreEqual(actual, expected);
        }
    }
}