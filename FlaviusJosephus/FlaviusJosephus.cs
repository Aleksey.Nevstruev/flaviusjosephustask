﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace FlaviusJosephus
{
    /// <summary>
    /// struct to enumerate in collection by flavius josephus
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct FlaviusJosephus<T>
    {
        private ICollection<T> collection;
        private List<int> fakeCollection;
        private int next;

        public FlaviusJosephus(ICollection<T> collection, int next)
        {
            this.next = next;
            this.collection = collection;
            this.fakeCollection = new List<int>();
            for (var i = 0; i < this.collection.Count; i++)
            {
                this.fakeCollection.Add(i);
            }
        }

        public IEnumerable<T> GetIEnumerable()
        {
            int length = fakeCollection.Count;
            int index = 0;
            while (length > 1)
            {
                index = (index + next - 1) % length;
                yield return this.collection.ElementAt(this.fakeCollection[index]);
                this.fakeCollection.RemoveAt(index);
                length--;
            }
        }
    }
}
